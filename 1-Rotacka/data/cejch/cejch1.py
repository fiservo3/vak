import numpy as np
import matplotlib.pyplot as plt


def mcleod(h, l):
	return((133*h*l)/(1100-l))

data = np.loadtxt("data.txt")

n=data.shape[0]

therm = data[:,2]
p=np.zeros_like(therm)

for i in range(n):
	p[i]=mcleod(data[i, 0], data[i, 1])


print(mcleod(3, 6))

np.save("../trubice/therm", therm)
np.save("../trubice/macleod", p)

plt.plot(therm, p, "+")
plt.xlabel("thermoclankovy")
plt.ylabel("p [Pa]")
plt.show()