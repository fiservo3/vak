import numpy as np
import matplotlib.pyplot as plt

pi=np.pi

def crop_t(t, p):
	return (t[:p.shape[0]], p)

def byreta(t):
	#enter time it took oil to get 5cm, reurns flow in cm^3/s
	p_atm = 1e5
	r = (0.246)/2
	h = 5
	s=pi*r**2
	V=s*h

	#DEBUG
	#print("tok" + str(V/t))
	return (p_atm*V/t)

tlak_zavreno=np.loadtxt("napoust_zavreno.txt")
tlak_1=np.loadtxt("napoust_1.txt")
tlak_2=np.loadtxt("napoust_2.txt")
tlak_3=np.loadtxt("napoust_3.txt")
#perioda mereni
t=30

byr1 = np.loadtxt("byreta_1.txt").mean()
byr2 = np.loadtxt("byreta_2.txt").mean()
byr3 = np.loadtxt("byreta_3.txt").mean()
byr4 = np.loadtxt("byreta_4.txt").mean()

q=byreta(np.array((byr1, byr2, byr3, byr4)))
p=np.array((4.9, 9.7, 15.7, 23))
Sef=q/p
n = tlak_zavreno.shape[0]

cas = t*np.arange(n)


for i in range(0, 4):
	print(str(p[i]) + " , " + str(q[i]) + " , " + str(Sef[i]) )


plt.plot(cas, tlak_zavreno, "x", color="blue", label = "ventil zavreny")

plt.plot(*crop_t(cas, tlak_1), linestyle="", marker="$✿$", color="orange", label ="mezni tlak 4.9 Pa")
plt.plot(*crop_t(cas, tlak_2), linestyle="", marker="$♥$", color="deeppink", label ="mezni tlak 9.7 Pa")
plt.plot(*crop_t(cas, tlak_3), linestyle="", marker="$❀$", color="green", label ="mezni tlak 15.7 Pa")
plt.xlabel("t [s]")
plt.ylabel("p [Pa]")
plt.semilogy()
#plt.yticks(ticks)
plt.grid()
plt.legend()
#plt.show()
plt.savefig("./vyvoj.png", format="png")
plt.close()
plt.plot(p, Sef, "x")

plt.xlabel("p [Pa]")
plt.ylabel("Sef [cm^3/s]")
plt.show()

