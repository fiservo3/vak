import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d


pi=np.pi

def mcleod(h, l):
	#enter values from mcLeod's vacuum gauge, returns rpessure in Pa
	return((133*h*l)/(1100-l))

def byreta(t):
	#enter time it took oil to get 5cm, reurns flow in cm^3/s
	p_atm = 1e5
	r = (0.246)/2
	h = 5
	s=pi*r**2
	V=s*h

	#DEBUG
	#print("tok" + str(V/t))
	return (p_atm*V/t)

cejch_therm=np.load("therm.npy")
cejch_macleod=np.load("macleod.npy")
kalibrace_thermoclanku=interp1d(cejch_therm, cejch_macleod, kind="cubic")
kalibrace_thermoclanku=interp1d(cejch_therm, cejch_macleod, kind="linear")

x_kalibr=np.linspace(cejch_therm[-1], cejch_therm[0], num=200, endpoint=True)

data=np.loadtxt("data.txt")
n=data.shape[0]

p1 = kalibrace_thermoclanku(data[:,2])
p2 = mcleod(data[:,0], data[:,1])

ps = (1/2)*(p1+p2)

q=byreta(data[:,3])
C=q/(p1-p2)

#and now compute C
vs= 485 #ms-1
d=8.5e-3
l=1
ls=lambda ps: 6.6e-3/ps
z=lambda d, ls: (2+2.507*(d/ls))/(2+3.095*(d/ls))
#z_dls= (2+2.507*(d/ls))/(2+3.095*(d/ls))

A=pi*(d/2)**2


Cv = 1.365*(d**4/l)*((p1+p2)/2)
Cm = (pi*d**3*vs)/(12*l)

F=lambda d, ls: (pi*d)/(128*ls)+(z(d, ls)/3)

Cmdt = 12.1*(d**3/l)

Cvm = lambda d, ls: A*(d/l)*F(d, ls)*vs #[m^3/s]
Cvm_scaled = lambda ps: Cvm(d, ls(ps))*1e6 #[cm^3/s]

#print("Cvm3: " + str(Cvm3))

plt.plot(cejch_therm, cejch_macleod, "rx", label="naměřené hodnoty")
plt.plot(x_kalibr, kalibrace_thermoclanku(x_kalibr), label="kalibrační křivka")
plt.xlabel("thermoclankovy")
plt.ylabel("p [Pa]")
plt.legend()
#plt.show()
plt.close()


xticks = np.array(( 5, 10, 20, 50))
xticks_l = np.array(( 5, 10, 20, 50))

x_ps = np.linspace(ps[0], ps[-1], num=200)

plt.plot(ps, C, "x", label="experiment")
plt.plot(x_ps, Cvm_scaled(x_ps), label="teorie")

#plt.loglog()
#plt.semilogx()
#plt.xticks(xticks, xticks_l)
plt.legend()

plt.ylabel("C [cm^3/s]")
plt.xlabel("p_s [Pa]")

#plt.show()

for i in range(0, 6):
	print (str(p1[i]) + " & " + str(p2[i]) + " & " + str(q[i]) + " & " + str(C[i]) + " & " + str(Cvm_scaled(ps[i])) )