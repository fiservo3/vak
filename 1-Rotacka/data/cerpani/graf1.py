import  numpy as np
import scipy.optimize
import math

tlak=np.loadtxt("napoust1.txt")
#perioda mereni
t=30

V=11.36

#tlak = data[:, 0]
n = tlak.shape[0]
cas = t*np.arange(n)

import matplotlib.pyplot as plt

def f(x, k, q):
	return (k*x+q)

def f_exp (x, k, q):
	return (np.exp(f(x, k, q)))

def crop_time(x, od, do):
	od_i = math.floor(od/t)
	do_i = math.ceil(do/t)
	return x[od_i:do_i]

od = 31
do = 211

cas_c =  crop_time(cas, od, do)
tlak_c =  crop_time(tlak, od, do)
param, covar = scipy.optimize.curve_fit(f, cas_c, np.log(tlak_c))
print("k = " + str(param[0]))
print("q = " + str(param[1]))

Sef= -param[0]*V
print("Sef = "+str(Sef) + "l/s")

plt.plot(cas, tlak, "x", color="blue", label="změřená data")
#plt.plot(cas, f(cas, *param), label="fit")
plt.plot(cas_c, f_exp(cas_c, *param), linestyle="-", linewidth=2, color="orange",label="fit")
plt.plot(cas, f_exp(cas, *param), linestyle="--", linewidth=.5, color="orange",)
plt.xlabel("t [s]")
plt.ylabel("p [Pa]")
plt.semilogy()
#plt.yticks(ticks)
plt.ylim(1e0, 1.5e5)
plt.legend()
plt.grid()
plt.show()
#plt.savefig("./vyvoj.png", format="png")

