import  numpy as np

tlak_bez=np.loadtxt("napoust1.txt")
tlak_otevrena=np.loadtxt("napoust_otevrena.txt")
tlak_s=np.loadtxt("napoust_proplach.txt")
#perioda mereni
t=30


k = tlak_s.shape[0]
n = tlak_bez.shape[0]

cas = t*np.arange(n)

import matplotlib
matplotlib.use("Qt5Agg")
import matplotlib.pyplot as plt

ticks=np.linspace(1.8e-2, 2.8e-2, 6)

plt.plot(cas, tlak_bez, "x", color="blue", label = "bez proplachovani")
plt.plot(cas[:tlak_s.shape[0]], tlak_s, "*", color="orange", label ="s proplachovanim")
plt.xlabel("t [s]")
plt.ylabel("p [Pa]")
plt.semilogy()
#plt.yticks(ticks)
plt.grid()
plt.legend()
plt.show()
#plt.savefig("./vyvoj.png", format="png")

