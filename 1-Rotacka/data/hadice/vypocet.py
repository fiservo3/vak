import numpy as np

from matplotlib import pyplot as plt
pi=np.pi

n=50

p_rec_a = 2.5
p_rec_b = 5
p_rec_c = 10
p_rec_d = 20

p1_a=np.ones(n)*p_rec_a
p2_a=np.linspace(0, p_rec_a, num=n)
ps_a = (1/2)*(p1_a+p2_a)

p1_b=np.ones(n)*p_rec_b
p2_b=np.linspace(0, p_rec_b, num=n)
ps_b = (1/2)*(p1_b+p2_b)

p1_c=np.ones(n)*p_rec_c
p2_c=np.linspace(0, p_rec_c, num=n)
ps_c = (1/2)*(p1_c+p2_c)

p1_d=np.ones(n)*p_rec_d
p2_d=np.linspace(0, p_rec_d, num=n)
ps_d = (1/2)*(p1_d+p2_d)

vs= 485 #ms-1
l=0.7 #cm
d=0.02 #cm
ls=lambda ps: 6.6e-3/ps
z=lambda d, ls: (2+2.507*(d/ls))/(2+3.095*(d/ls))
#z_dls= (2+2.507*(d/ls))/(2+3.095*(d/ls))

A=pi*(d/2)**2



F=lambda d, ls: (pi*d)/(128*ls)+(z(d, ls)/3)


Cvm = lambda d, ls: A*(d/l)*F(d, ls)*vs #[m^3/s]
Cvm_scaled = lambda ps: Cvm(d, ls(ps))*1e6 #[cm^3/s]

plt.plot(p2_a, Cvm_scaled(ps_a), label="p1 = 2.5 Pa")
plt.plot(p2_b, Cvm_scaled(ps_b), label="p1 = 5 Pa")
plt.plot(p2_c, Cvm_scaled(ps_c), label="p1 = 10 Pa")
plt.plot(p2_d, Cvm_scaled(ps_d), label="p1 = 20 Pa")

plt.ylabel("C [cm^3/s]")
plt.xlabel("p_2 [Pa]")

plt.legend()
plt.show()


