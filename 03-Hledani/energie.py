mu = 1.660539040e-27
ar = 4.002602 

m=mu*ar

print("m [kg] = "+str(m))

B= 140e-3
q=1.6021766208e-19

r= 40e-3

e_j = (B**2 * q**2 * r**2)/(2 * m)
print("T [J] = "+str(e_j))

#jeden eV v joulech
ev=1.6021766208e-19

e_ev=e_j/ev

print("T [eV] = "+str(e_ev))
