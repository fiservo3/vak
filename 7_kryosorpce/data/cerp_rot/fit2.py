import  numpy as np
import scipy.optimize
import math

kombi=np.loadtxt("kombi.txt")
kryo=np.loadtxt("2kryo.txt")


import matplotlib.pyplot as plt


t_r = kombi[42:, 0]
p_r = kombi[42:, 1]


t_k = kryo[67:, 0]
p_k = kryo[67:, 1]


def f(x, a, k, b):
	res=a*np.exp(k*x) + b
	#print(res)
	return(res)


extrap_time =7000

param_r, covar_r = scipy.optimize.curve_fit(f, t_r, p_r, p0=(1, -0.001, -0.1))
param_k, covar_k = scipy.optimize.curve_fit(f, t_k, p_k, p0=(1, -0.001, -0.1))


mezni_r =param_r[2]
mezni_k =param_k[2]

print("kombinovane: " + str(mezni_r) + "+-" + str(covar_r[2,2]))
print("dvoj kryo: " + str(mezni_r) + "+-" + str(covar_k[2,2]))


#t_r_ext =np.concatenate([t_r, np.logspace(1000, extrap_time)])
long_time = np.logspace(math.log(t_r[0], 10), math.log(extrap_time, 10))
long_time_k = long_time[11:]

ticks=np.array([1., 1,5, 2., 3,  5., 7])*1e3

ytics =[1e-1, mezni_r, mezni_k, 4e0]


plt.plot(t_r, p_r, "x", label="Kombinovane", color="green")
plt.plot(t_k, p_k, "o", label="dvoustupnova kryosorpce", color="red")
plt.plot(long_time, f(long_time, *param_r), label="dvoustupnova fit", linewidth="0.8", color="green")
plt.plot(long_time_k, f(long_time_k, *param_k), label="dkombinovana fit", linewidth="0.8", color="red")

plt.xlabel("t [s]")
plt.ylabel("p [Pa]")
plt.loglog()
plt.ylim(1e-1, 4e0)
plt.xlim(right=extrap_time)
plt.xticks(ticks)
#plt.yticks(ytics, "{0:.2f}{0:.2f}{0:.2f}{0:.2f}".format(ytics))

plt.axhline(mezni_k, linewidth = 0.5, color="red")
plt.axhline(mezni_r, linewidth = 0.5, color="green")

plt.legend()
plt.grid()
plt.show()
#plt.savefig("./vyvoj.png", format="png")


#protokol dat sekretarce
#Protokol oznacit KFE FJFI, Kral, hodit na vratnici tezkych laboraori
