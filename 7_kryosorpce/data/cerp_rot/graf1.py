import  numpy as np
import scipy.optimize
import math

kombi=np.loadtxt("kombi.txt")
kryo=np.loadtxt("2kryo.txt")


import matplotlib.pyplot as plt

t_k = kombi[:, 0]
p_k = kombi[:, 1]


t_2k = kryo[:, 0]
p_2k = kryo[:, 1]

plt.plot(t_k, p_k, label="Kombinovane")
plt.plot(t_2k, p_2k, label="dvoustupnova kryosorpce")

plt.xlabel("t [s]")
plt.ylabel("p [Pa]")
plt.semilogy()
#plt.yticks(ticks)

plt.legend()
plt.grid()
plt.show()
#plt.savefig("./vyvoj.png", format="png")


#protokol dat sekretarce
#Protokol oznacit KFE FJFI, Kral, hodit na vratnici tezkych laboraori
