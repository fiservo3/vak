set terminal pdfcairo
set output "fit.pdf"
data='bez_clonky.txt'

k=1
q=1

f(x)=k*x+q

fit f(x) data using 1:2 via k,q

plot \
	data using 1:2 title "data",\
	f(x) title "fit"
