#Clonka: 5mm, l=1
#jehlovy venil je u penningu

import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize


data_bez=np.loadtxt("bez_clonky.txt")
data_s=np.loadtxt("s_clonkou.txt")


#Cejchovani Penningu

ioniz=data_bez[:, 0]*133.322
penning=data_bez[:, 1]

#tics=np.linspace(1e-6, 1.6e-6, 7)
print(ioniz)


def f(x, k, q):
	return (k*x+q)

popt, pcov = scipy.optimize.curve_fit(f, ioniz, penning)

print(pcov)

plt.plot(ioniz, penning, "x")
plt.plot(ioniz, f(ioniz, *popt))



plt.xlabel("p [Pa] podle ionizačního vakuometru")
plt.ylabel("p [Pa]")
#plt.yticks(tics)
plt.show()