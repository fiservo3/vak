import  numpy as np
import scipy.optimize
import matplotlib.pyplot as plt


data=np.loadtxt("bez_clonky.txt")

ioniz = data[:, 0]*133.322
penning = data[:, 1]


def f(x, k, q):
	return k*x+q

koeficienty, kovariance = scipy.optimize.curve_fit(f, penning, ioniz)

print(koeficienty)

def tlak(penn):
	spocteny = f(penning, *koeficienty)
	return spocteny

plt.plot(penning, ioniz, "+", color="green", label="data")
plt.plot(penning, tlak(penning), label="f(x)=kx+q")

plt.xlabel("p2 [-]")
plt.ylabel("p1 [Pa]")

plt.legend()

d=0.5
A=np.pi*d**2

C0=11.6*A
W = 0.83
C=C0*W

print(str(C)+"l/s")

plt.show()

clonka = np.loadtxt("s_clonkou.txt")

p1=clonka[:, 0]*133.322
p2=np.zeros_like(p1)

n=clonka.shape[0]

print(type(clonka[1, 1]))

for k in range(0, n):

	p2[k] = ( f(clonka[k, 1], *koeficienty))

q = np.zeros_like(p1)
S= np.zeros_like(p1)
Sef = np.zeros_like(p1)

for k in range(0, n):
	q[k] = C*(p2[k]-p1[k])
	S[k] = q[k]/p1[k]
	Sef[k] = (C*S[k])/(S[k]+C)



print(q)
print(S)
print(Sef)


fig, ax1 = plt.subplots()



color = 'orange'

ax1.set_xlabel('p1 [Pa]')
ax1.set_ylabel('S [l/s]', color=color)
ax1.plot(p1, S, "+", color=color, label="S")
ax1.plot(p1, Sef, "x", color=color, label="Sef")
ax1.tick_params(axis='y', labelcolor=color)


ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

color = 'tab:blue'
ax2.set_ylabel('q [Pa l / s]', color=color)  # we already handled the x-label with ax1
ax2.plot(p1, q, "*", color=color, label="q")
ax2.tick_params(axis='y', labelcolor=color)

fig.tight_layout()  # otherwise the right y-label is slightly clipped
fig.legend(loc=(0.15,0.8))
ax1.grid(axis="y")
plt.show()


