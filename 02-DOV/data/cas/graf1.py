import numpy as np
import matplotlib.pyplot as plt


data=np.loadtxt("data3.txt")

y=data[:, 0]*133.322
x=data[:, 1]

tics=np.linspace(0.019, 0.028, 10)
print(y)

plt.plot(x, y, "x")
plt.semilogy()

plt.xlabel("t [m]")
plt.ylabel("p [Pa]")
plt.yticks(tics)
plt.show()