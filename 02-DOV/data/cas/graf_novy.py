import  numpy as np

data=np.loadtxt("data3.txt")


tlak = data[:, 0]*133.322
cas = data[:, 1]

import matplotlib.pyplot as plt

ticks=np.linspace(1.8e-2, 2.8e-2, 6)

plt.errorbar(cas, tlak, yerr=3e-4, fmt="x", color="blue")
plt.xlabel("t [min]")
plt.ylabel("p [Pa]")
plt.semilogy()
plt.yticks(ticks)
plt.grid()

plt.savefig("./vyvoj.png", format="png")

