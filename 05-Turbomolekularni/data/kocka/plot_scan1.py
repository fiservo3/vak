import numpy as np
from matplotlib import pyplot as plt
import scipy.interpolate


from peaks import peaks

dm=1/32

data=np.loadtxt("scan1.asc")

m = data[:,0]
p = data[:,1]

parc = scipy.interpolate.interp1d(m, p, kind="cubic")

n=peaks.shape[0]

def around(x):
	sigma =0.15
	num=20
	return (np.linspace(x-sigma, x+sigma, num=num, endpoint=True))

print(type(peaks[:,0].astype(np.float)[1]))
peak_values = np.zeros_like(peaks[:,0])
for i in range(n):
	peak_values[i] = max(parc(around(peaks[i, 0].astype(np.float))))


print(peaks)

#let n be # of peaks

for i in range(n):
	print("m: " + str(peaks[i, 0]) + ", p: " + str(peak_values[i]))



#plot measured spectrum
plt.plot(m, p)

#and now highlite peaks
for i in range(n):
	plt.plot(float(peaks[i, 0]), float(peak_values[i]), "o", color=peaks[i, 2], label=peaks[i, 1])



plt.xlabel("Ar/C")
plt.ylabel("p parciální [Pa]")

plt.semilogy()

plt.ylim(1e-9, 1e-4)
plt.xlim(0,55)
plt.grid()
plt.legend()
plt.show()