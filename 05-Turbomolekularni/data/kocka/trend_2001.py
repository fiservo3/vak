import numpy as np
from matplotlib import pyplot as plt
import scipy.interpolate

data=np.loadtxt("trend1.asc")

t_H = data[:, 0]
H = data[:, 1]
t_He = data[:, 2]
He = data[:, 3]
t_H2O = data[:, 4]
H2O = data[:, 5]
t_O2 = data[:, 6]
O2 = data[:, 7]
t_OH = data[:, 8]
OH = data[:, 9]
t_N2 = data[:, 10]
N2 = data[:, 11]
t_Ar = data[:, 12]
Ar = data[:, 13]


plt.plot(t_H, H, label="H")
plt.plot(t_He, He, label="He")
plt.plot(t_H2O, H2O, label="H2O")
plt.plot(t_O2, O2, label="O2")
plt.plot(t_OH, OH, label="OH")
plt.plot(t_N2, N2, label="N2")
plt.plot(t_Ar, Ar, label="Ar")

plt.semilogy()
plt.legend()

plt.xlabel("t [s]")
plt.ylabel("p parciální [Pa]")

plt.show()