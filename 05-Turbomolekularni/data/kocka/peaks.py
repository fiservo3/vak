import numpy as np

peaks = np.array((	(1., "H+" 	, "purple"),
					(2., "H2"	, "deeppink"),
					(8., "He2"	, "slateblue"),
					(12., "C"	, "blue"),
					(14., "N"	, "dodgerblue"),
					(15., "NH"	, "cyan"),
					(16., "O"	, "springgreen"),
					(17., "OH-"	, "green"),
					(18., "H2O"	, "yellow"),
					(19., "?"	, "orange"),
					(28., "N2"	, "coral"),
					(32., "O2"	, "red"),
					(44., "CO2"	, "darkred")
					))


Colors=("purple",
		"deeppink",
		"slateblue",
		"blue",
		"dodgerblue",
		"cyan",
		"springgreen",
		"green",
		"yello",
		"orange",
		"coral",
		"red",
		"darkred",
		)		

