import numpy as np
from matplotlib import pyplot as plt
import scipy.signal
import scipy.interpolate

from smooth import smooth




data = np.loadtxt("data.txt")

time = data[:, 0]
pressure = data[:, 1]

pressure=scipy.signal.medfilt(pressure, kernel_size=5)

p_interp=scipy.interpolate.interp1d(time, pressure, kind="cubic")

pressure = p_interp(time)

pressure = smooth(pressure, window_len=20,window='blackman')

k=time.shape[0]
n=pressure.shape[0]

crop = int((n-k)/2)

print("cropping pressure on both sides by " + str(crop))
pressure = pressure[crop:n-crop-1]
plt.plot(time, pressure)
plt.semilogy()
plt.ylim((4e-5, 1.2e5))
plt.grid()
plt.show()