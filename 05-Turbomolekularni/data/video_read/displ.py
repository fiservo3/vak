def decode(x):
    return  {"1110111": 0,
             "0010010": 1,
             "1011101": 2, 
             "1011011": 3, 
             "0111010": 4, 
             "1101011": 5, 
             "1101111": 6, 
             "1010010": 7, 
             "1111111": 8,
             "1111011": 9,
            }.get(x, float("nan"))

def read_cipher(frame, coordinates, threshold=100.):
    s=""
    for i in range(7):
        green = (frame[coordinates[i][1], coordinates[i][0], 1])
        if green >= threshold:
            s+="1"
        else:
            s+="0"
    return (s)

def read_sign(frame, coordinates, threshold=100.):
    if(frame[coordinates[1], coordinates[0], 1] >= threshold):
        return True
    else:
        return False

def assemble_number(x, y, s, e):
    mantissa = x + 0.1*y
    exponent = e if s else (-1)*e
    return (mantissa*10**exponent)