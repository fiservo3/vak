import numpy as np

import scipy.optimize
import scipy.linalg

import cv2

from matplotlib import pyplot as plt 

from coor import x, y
from displ import read_frame, decode, assemble_number


colors=("violet",
		"blue",
		"lightblue",
		"green",
		"yellow",
		"orange",
		"red")




videofile = cv2.VideoCapture("../video/VID_20181211_154341.mp4")

print(type(videofile))
#print(videofile.shape())

for i in range(1000):
	ret, frame = videofile.read()
	a = decode(read_frame(frame, x))
	b = decode(read_frame(frame, y))

	print(assemble_number(a, b))

plt.imshow(frame)

for i in range(7):
	plt.plot(*x[i], "x", color=colors[i], label="x " + str(i))

for i in range(7):
	plt.plot(*y[i], "+", color=colors[i], label="y" + str(i))




plt.legend()
plt.show()
