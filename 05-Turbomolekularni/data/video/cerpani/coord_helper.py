import numpy as np

import scipy.optimize
import scipy.linalg

import cv2

from matplotlib import pyplot as plt 


from coor import x, y, s, e
from displ import read_cipher, read_sign, decode, assemble_number


colors=("violet",
		"blue",
		"lightblue",
		"green",
		"yellow",
		"orange",
		"red")



videofile = cv2.VideoCapture("./video.mp4")
fps = videofile.get(cv2.CAP_PROP_FPS)
t  = 1./fps
n = int(videofile.get(cv2.CAP_PROP_FRAME_COUNT))
dur = n/fps


position = 11.25

frame_no = int(position*fps)

print(frame_no)
videofile.set(1, frame_no)

while 1:
	
	ret, frame = videofile.read()
	
	
	if not ret:
		print("could not read frame")
		exit()
	

	print()
	print()
	print()
	print()
	print("Frame: " + str(videofile.get(1,)) + ", Time: " + str(videofile.get(1,)/fps))

	print("X:")
	print(read_cipher(frame, x, threshold=210))
	print(decode(read_cipher(frame, x, threshold=210)))
	
	
	
	print("Y:")
	print(read_cipher(frame, y, threshold=210))
	print(decode(read_cipher(frame, y, threshold=210)))
	
	plt.imshow(frame)
	
	for i in range(7):
		plt.plot(*x[i], "x", color=colors[i], label="x" + str(i))
	
	for i in range(7):
		plt.plot(*y[i], "+", color=colors[i], label="y" + str(i))
	
	print("sign:")
	print(read_sign(frame, s, threshold=210))


	
	print("E:")
	print(read_cipher(frame, e, threshold=210))
	print(decode(read_cipher(frame, e, threshold=210)))
	
	
	plt.legend()
	plt.show()
	
	plt.close()

#plt.imshow(frame)
#
#for i in range(7):
#	plt.plot(*e[i], "x", color=colors[i], label="e" + str(i))
#	
#plt.plot(*s, "+", color="deeppink", label="s")
#
#plt.legend()
#plt.show()

