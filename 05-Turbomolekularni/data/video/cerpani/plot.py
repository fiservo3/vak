import numpy as np
from matplotlib import pyplot as plt
import scipy.signal
import scipy.interpolate

from smooth import smooth




data = np.loadtxt("data.txt")

#purge bad data (containing nans)

#First, let's keep the original time data. They does not containt errors

#


while np.isnan(data[0,1]):
	data = np.delete(data, (0), axis=0)


while np.isnan(data[-1,1]):
	data = np.delete(data, (data.shape[0]-1), axis=0)

time_original = data[:, 0]

data = data[~np.isnan(data[:, 1])]

time = data[:, 0]
pressure = data[:, 1]
pressure=scipy.signal.medfilt(pressure, kernel_size=13)

print(data[0,:])
print(data[-1,:])

print(data[-1,:])

#simple median filtering
#helps cut misdetected values

#and now make up purged values
#I will use 
p_interp=scipy.interpolate.interp1d(time, pressure, kind="cubic")

pressure = p_interp(time_original)

#pressure_smoothed = smooth(pressure, window_len=6000,window='blackman')

k=time_original.shape[0]
n=pressure.shape[0]

crop = int((n-k))

#print("cropping pressure on both sides by " + str(crop))
#pressure_smoothed = pressure_smoothed[]
plt.plot(time_original/60, pressure)
#plt.plot(time_original/60, pressure_smoothed)
plt.semilogy()
#plt.ylim((4e-5, 1.2e5))
plt.grid()
plt.xlabel("t [min]")
plt.ylabel("p [Pa]")
plt.show()