import numpy as np

import scipy.optimize
import scipy.linalg
import scipy.signal

import cv2


from coor import x, y, s, e
from displ import read_cipher, read_sign, decode, assemble_number


videofile = cv2.VideoCapture("./video.mp4")
fps = videofile.get(cv2.CAP_PROP_FPS)
t  = 1./fps
n = int(videofile.get(cv2.CAP_PROP_FRAME_COUNT))

print("duration = " + str(n/fps))
print("duration = " + str(n*t/60))

print(t)
print(n)


time = np.arange(0, n)*t
pressure = np.zeros_like(time)

print (time[-1])

#exit()
print("total frames: " + str(n))
#print(videofile.shape())

counter_x = np.zeros(10)

counter_y = np.zeros(10)
counter_e = np.zeros(10)

threshold = 1

ret = 1
frame_counter = 0

while 1:


	ret, frame = videofile.read()		
	if ret:
		a = decode(read_cipher(frame, x, threshold=210))
		b = decode(read_cipher(frame, y, threshold=210))
		c = read_sign(frame, s, threshold=80)
		d = decode(read_cipher(frame, e, threshold=210))


		num = assemble_number(a, b, c, d)

		percent = 100 * frame_counter/n
		print( "{:.1f}".format(percent) + "%, frame: " + str(frame_counter) + ", value: " + str(num))

		pressure[frame_counter] = num

		frame_counter += 1

	else:
		print("reading finished")
		break


np.savetxt("data.txt", np.stack((time, pressure), axis=-1))

from matplotlib import pyplot as plt


scipy.signal.medfilt(pressure, kernel_size=15)

plt.plot(time, pressure)
plt.semilogy()
plt.ylim((4e-5, 1.2e5))
plt.grid()
plt.show()